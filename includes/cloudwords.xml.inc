<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Utility class containing methods to encapsulate a node element into a customer xml schema.
 * 
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */
class XMLUtility {

  private function add_child_xml_element($dom, $root, $elem_name, $elem_value) {
    if( $elem_name ) {
      $item = $dom->createElement($elem_name);
      $text = $dom->createTextNode($elem_value ? $elem_value : '');
      $item->appendChild($text);
      $root->appendChild($item);
    }
  }

  private function add_child_comment_element($root, $message) {
    $comment = new DOMComment($message);
    $root->appendChild($comment);
  }
  
  private function generate_node_xml($node) {
    $dom = new DOMDocument("1.0");
    $dom->formatOutput = true;
    $root = $dom->createElement("node");
    $dom->appendChild($root);

    // add section for fields that should be translated
    $this->add_child_comment_element($root, " FIELDS IN THIS SECTION CAN BE TRANSLATED ");
    $this->add_child_xml_element($dom, $root, "title", $node['title']);
    $this->add_child_xml_element($dom, $root, "body", $node['body']);
    $this->add_child_xml_element($dom, $root, "teaser", $node['teaser']);

    // add section for fields that should not be translated
    $this->add_child_comment_element($root, " DO NOT TRANSLATE ANY OF THE FIELDS BELOW THIS LINE ");
    $this->add_child_xml_element($dom, $root, "nid", $node['nid']);
    $this->add_child_xml_element($dom, $root, "type", $node['type']);
    $this->add_child_xml_element($dom, $root, "language", $node['language']);
    $this->add_child_xml_element($dom, $root, "uid", $node['uid']);
    $this->add_child_xml_element($dom, $root, "status", $node['status']);
    $this->add_child_xml_element($dom, $root, "created", $node['created']);
    $this->add_child_xml_element($dom, $root, "changed", $node['changed']);
    $this->add_child_xml_element($dom, $root, "comment", $node['comment']);
    $this->add_child_xml_element($dom, $root, "promote", $node['promote']);
    $this->add_child_xml_element($dom, $root, "moderate", $node['moderate']);
    $this->add_child_xml_element($dom, $root, "sticky", $node['sticky']);
    $this->add_child_xml_element($dom, $root, "tnid", $node['tnid']);
    $this->add_child_xml_element($dom, $root, "translate", $node['translate']);
    $this->add_child_xml_element($dom, $root, "vid", $node['vid']);
    $this->add_child_xml_element($dom, $root, "revision_uid", $node['revision_uid']);
    $this->add_child_xml_element($dom, $root, "log", $node['log']);
    $this->add_child_xml_element($dom, $root, "revision_timestamp", $node['revision_timestamp']);
    $this->add_child_xml_element($dom, $root, "format", $node['format']);
    $this->add_child_xml_element($dom, $root, "name", $node['name']);
    $this->add_child_xml_element($dom, $root, "picture", $node['picture']);
    $this->add_child_xml_element($dom, $root, "data", $node['data']);
    $this->add_child_xml_element($dom, $root, "last_comment_timestamp", $node['last_comment_timestamp']);
    $this->add_child_xml_element($dom, $root, "last_comment_name", $node['last_comment_name']);
    $this->add_child_xml_element($dom, $root, "comment_count", $node['comment_count']);
    $this->add_child_xml_element($dom, $root, "taxonomy", $node['taxonomy']);
    return $dom->saveXML();
  }

  public function validate_schema($files, $schema) {
    foreach( $files as $file ) {
      if( !is_dir($file) ) {
        $doc = DOMDocument::loadXML( file_get_contents($file) );
        $valid = $doc->schemaValidate($schema);
        if( !$valid ) {
          $params = array('error_message' => "The translated materials cannot be imported because they are not of the proper format. Please ensure that the vendor has retained the original format in the translated materials.");
          throw new CloudwordsXmlException(CloudwordsXmlException::XML_EXCEPTION, $params);
        }
      }
    }      
  }

  public function extract_xml_node_data($filename, $target_language_code) {
    $xml = simplexml_load_file($filename);
    $node = array();
    $node['nid'] = (string)$xml->nid;
    $node['type'] = (string)$xml->type;
    $node['language'] = $target_language_code;
    $node['uid'] = (string)$xml->uid;
    $node['status'] = (string)$xml->status;
    $node['created'] = (string)$xml->created;
    $node['changed'] = (string)$xml->changed;
    $node['comment'] = (string)$xml->comment;
    $node['promote'] = (string)$xml->promote;
    $node['moderate'] = (string)$xml->moderate;
    $node['sticky'] = (string)$xml->sticky;
    $node['tnid'] = (string)$xml->tnid;
    $node['translate'] = (string)$xml->translate;
    $node['vid'] = (string)$xml->vid;
    $node['revision_uid'] = (string)$xml->revision_uid;
    $node['title'] = (string)$xml->title;
    $node['body'] = (string)$xml->body;
    $node['teaser'] = (string)$xml->teaser;
    $node['log'] = (string)$xml->log;
    $node['revision_timestamp'] = (string)$xml->revision_timestamp;
    $node['format'] = (string)$xml->format;
    $node['name'] = (string)$xml->name;
    $node['picture'] = (string)$xml->picture;
    $node['data'] = (string)$xml->data;
    $node['last_comment_timestamp'] = (string)$xml->last_comment_timestamp;
    $node['last_comment_name'] = (string)$xml->last_comment_name;
    $node['comment_count'] = (string)$xml->comment_count;
    $node['taxonomy'] = (string)$xml->taxonomy;
    return $node;
  }

  public function serialize_node_contents($node) {
    $xml = $this->generate_node_xml($node);
    $filename = tempnam('', 'node-') . '-' . $node['nid'] . '.xml';
    file_put_contents($filename, $xml);
    return $filename;
  }

}

/**
 * Exception thrown when a call to the Cloudwords XML utility returns an exception.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */
class CloudwordsXmlException extends Exception {

  const XML_EXCEPTION = 'xml_exception';

  private $exception_type;

  public function __construct($exception_type, $params) {
    $this->exception_type = $exception_type;
    if( $exception_type == self::XML_EXCEPTION) {
      $this->error_message = $params['error_message'];
    }
  }

  public function getExceptionType() {
    return $this->exception_type;
  }

  public function getErrorMessage() {
    return $this->error_message;
  }

  public function __toString() {
    if( $this->exception_type == self::XML_EXCEPTION) {
      return $this->error_message . "\n";
    }
  }

}

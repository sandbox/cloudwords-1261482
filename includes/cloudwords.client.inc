<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Convenience method that returns an instance of the api client using the 
 * configuration settings in the module administration settings page.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

function get_api_client() {
  $dao = new CloudwordsDao();
  $settings = $dao->get_configuration_settings();
  $base_api_url = $settings['base_api_url'];
  $api_version = $settings['api_version'];
  $auth_token = 'UserToken ' . $settings['auth_token'];
  return new CloudwordsClient($base_api_url, $api_version, $auth_token);
}

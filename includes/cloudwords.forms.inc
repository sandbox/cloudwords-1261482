<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Form rendering functions.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

require_once 'cloudwords.controllers.inc';
require_once 'cloudwords.errors.inc';
require_once 'cloudwords.themes.inc';

function admin_settings_form() {
  show_custom_messages($form);
  $dao = new CloudwordsDao();
  $config = $dao->get_configuration_settings();
  $form['admin_settings'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fieldset-title">' . t('Configuration Settings') . '</span>',
    '#tree' => TRUE,
  );
  $form['admin_settings']['base_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base API URL'),
    '#description' => t('The base URL to the Cloudwords API'),
    '#size' => 64,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#default_value' => !empty($config['base_api_url']) ? $config['base_api_url'] : DEFAULT_BASE_API_URL,
  );
  $form['admin_settings']['api_version'] = array(
    '#type' => 'textfield',
    '#title' => t('API version'),
    '#description' => t('The version number of the Cloudwords API (Note: This module has only been tested using version 1 of the Cloudwords API)'),
    '#size' => 64,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#default_value' => !empty($config['api_version']) ? $config['api_version'] : DEFAULT_API_VERSION,
  );
  $form['admin_settings']['auth_token'] = array(
    '#type' => 'textfield',
    '#title' => t('API Authorization Token'),
    '#description' => t('The API authorization token generated by Cloudwords'),
    '#size' => 64,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#default_value' => $config['auth_token'],
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Configuration'));
  return $form;
}

function create_project_form() {
  show_custom_messages($form);
  try {
    $client = get_api_client();
    $source_languages = $client->get_source_languages();
    $target_languages = $client->get_target_languages();
    $intended_uses = $client->get_intended_uses();

    import_date_picker_widget();
    $form['project_details'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['project_details']['project_name'] = array(
      '#type' => 'textfield',
      '#title' => t('What do you want to call this project?'),
      '#size' => 30,
      '#maxlength' => 64,
    );
    $form['project_details']['bid_due_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Deadline for receiving quotes'),
      '#size' => 30,
      '#maxlength' => 64,
      '#id' => 'datepicker-bid-due-date',
    );
    $form['project_details']['delivery_due_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Deadline for deliverables'),
      '#size' => 30,
      '#maxlength' => 64,
      '#id' => 'datepicker-delivery-due-date',
    );
    $form['project_details']['purchase_order_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Purchase Order Number'),
      '#size' => 30,
      '#maxlength' => 64,
    );
    $form['project_details']['source_language'] = array(
      '#type' => 'select',
      '#title' => t('What is your source language?'),
      '#options' => _get_language_options($source_languages),
    );
    $options = _get_language_options($target_languages);
    $form['project_details']['target_languages'] = array(
      '#type' => 'select',
      '#title' => t('What are your target languages?'),
      '#options' => $options,
      '#multiple' => true,
      '#size' => min(12, count($options)),
    );
    $form['project_details']['intended_use'] = array(
      '#type' => 'select',
      '#title' => t('What is the intended use of this project?'),
      '#options' => _get_intended_use_options($intended_uses),
    );
    $form['project_details']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Provide a brief description for your project (250 characters allowed)'),
      '#resizable' => false,
    );
    $form['project_details']['notes'] = array(
      '#type' => 'textarea',
      '#title' => t('Notes and instructions for the vendors. Use this field to provide specific instructions to the vendor about their quotes, the way to prepare your project deliverables, etc. (1,000 characters allowed'),
      '#resizable' => false,
    );
    $form['submit'] = array('#type' => 'submit', '#value' => t('Create Project'));
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
  }
  return $form;
}

function view_projects_form() {
  show_custom_messages($form);
  try {
    $client = get_api_client();
    $dao = new CloudwordsDao();
    $open_projects = $dao->filter_projects($client->get_open_projects());
    $closed_projects = $dao->filter_projects($client->get_closed_projects());
    // display a list of open projects
    $form['open_projects'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fieldset-title">' . t('Open Projects') . '</span>',
      '#tree' => TRUE,
    );
    foreach( $open_projects as &$project ) {
      $project_id = $project->getId();
      $open_projects_list[$project_id] = '';
      $form['open_projects'][$project_id]['project_name'] = array('#value' => $project->getName());
      $form['open_projects'][$project_id]['project_status'] = array('#value' => $project->getStatus()->getDisplay());
    }
    $form['open_projects']['project_list'] = $open_projects_list;
    $form['open_projects']['#theme'] = 'project_list_theme';
    // display a list of closed projects
    $form['closed_projects'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fieldset-title">' . t('Closed Projects') . '</span>',
      '#tree' => TRUE,
    );
    foreach( $closed_projects as &$project ) {
      $project_id = $project->getId();
      $closed_projects_list[$project_id] = '';
      $form['closed_projects'][$project_id]['project_name'] = array('#value' => $project->getName());
      $form['closed_projects'][$project_id]['project_status'] = array('#value' => $project->getStatus()->getDisplay());
    }
    $form['closed_projects']['project_list'] = $closed_projects_list;
    $form['closed_projects']['#theme'] = 'project_list_theme';
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
  }
  return $form;
}

function project_details_form($form_state, $project_id) {
  show_custom_messages($form);
  $client = get_api_client();
  try {
    $project = $client->get_project($project_id);
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
  }
  try {
    $source_file = $client->get_project_source($project_id);
  } catch( CloudwordsApiException $exc ) {
    $source_file = NULL;
  }
  try {
    $reference_files = $client->get_project_references($project_id);
  } catch( CloudwordsApiException $exc ) {
    $reference_files = array();
  }
  try {
    $master_translated_file = $client->get_master_project_translated_file($project_id);
  } catch( CloudwordsApiException $exc ) {
    $master_translated_file = NULL;
  }
  try {
    $translated_files = $client->get_project_translated_files($project_id);
  } catch( CloudwordsApiException $exc ) {
    $translated_files = array();
  }

  // project details
  if( !is_null($project) ) {
    $form['project_details'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fieldset-title">' . t('Project Details') . '</span>',
      '#tree' => TRUE,
    );
    $form['project_details']['metadata']['project_id'] = array('#value' => $project->getId());
    $form['project_details']['metadata']['description'] = array('#value' => $project->getDescription());
    $form['project_details']['metadata']['status'] = array('#value' => $project->getStatus()->getDisplay());
    $form['project_details']['metadata']['status_code'] = array('#value' => $project->getStatus()->getCode());
    $form['project_details']['metadata']['intended_use'] = array('#value' => $project->getIntendedUse()->getName());
    $form['project_details']['metadata']['amount'] = array('#value' => '$' . $project->getAmount());
    $form['project_details']['metadata']['purchase_order_number'] = array('#value' => $project->getPoNumber());
    $form['project_details']['metadata']['source_language'] = array('#value' => $project->getSourceLanguage()->getDisplay());
    $form['project_details']['metadata']['target_languages'] = array('#value' => $project->getTargetLanguagesDisplay());
    $form['project_details']['metadata']['notes'] = array('#value' => $project->getNotes());
    $form['project_details']['metadata']['created_date'] = array('#value' => _format_display_date($project->getCreatedDate()));
    $form['project_details']['metadata']['bid_due_date'] = array('#value' => _format_display_date($project->getBidDueDate()));
    $form['project_details']['metadata']['bid_selection_deadline'] = array('#value' => _format_display_date($project->getBidSelectDeadlineDate()));
    $form['project_details']['metadata']['delivery_due_date'] = array('#value' => _format_display_date($project->getDeliveryDueDate()));
    $form['project_details']['#theme'] = 'project_details_theme';
  }

  // project source materials
  $form['project_source_materials'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fieldset-title">' . t('Source Materials') . '</span>',
    '#tree' => TRUE,
  );
  if( !is_null($source_file) ) {
    $dao = new CloudwordsDao();
    $source_nodes = $dao->get_project_nodes($project_id);
    foreach( $source_nodes as $node_id ) {
      $node = $dao->get_node_by_id($node_id);
      $source_node_list[$node_id] = '';
      $form['project_source_materials']['source_node'][$node_id]['title'] = array('#value' => $node['title']);
      $form['project_source_materials']['source_node'][$node_id]['language'] = array('#value' => $client->lookup_language($node['language']));
      $form['project_source_materials']['source_node'][$node_id]['type'] = array('#value' => $node['type']);
      $form['project_source_materials']['source_node'][$node_id]['author'] = array('#value' => $node['name']);
    }
  }
  $form['project_source_materials']['project_id'] = array('#value' => $project->getId());
  $form['project_source_materials']['source_nodes'] = $source_node_list;
  $form['project_source_materials']['#theme'] = 'project_source_materials_theme';

  // project reference materials
  $form['project_reference_materials'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fieldset-title">' . t('Reference Materials') . '</span>',
    '#tree' => TRUE,
  );
  foreach( $reference_files as &$reference_file ) {
    $file_id = $reference_file->getId();
    $reference_file_list[$file_id] = '';
    $form['project_reference_materials']['reference_file'][$file_id]['filename'] = array('#value' => $reference_file->getFilename());
    $form['project_reference_materials']['reference_file'][$file_id]['file_size'] = array('#value' => $reference_file->getSizeInKilobytes());
    $form['project_reference_materials']['reference_file'][$file_id]['file_contents'] = array('#value' => $reference_file->getFileContents());
    $form['project_reference_materials']['reference_file'][$file_id]['created_date'] = array('#value' => _format_display_date($reference_file->getCreatedDate()));
    $form['project_reference_materials']['reference_file'][$file_id]['metadata_path'] = array('#value' => $reference_file->getPath());
    $form['project_reference_materials']['reference_file'][$file_id]['content_path'] = array('#value' => $reference_file->getContentPath());
  }
  $form['project_reference_materials']['project_id'] = array('#value' => $project->getId());
  $form['project_reference_materials']['reference_files'] = $reference_file_list;
  $form['project_reference_materials']['#theme'] = 'project_reference_materials_theme';

  // project deliverables
  $form['project_deliverables'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fieldset-title">' . t('Deliverables') . '</span>',
    '#tree' => TRUE,
  );
  foreach( $translated_files as &$translated_file ) {
    $file_id = $translated_file->getId();
    $translated_file_list[$file_id] = '';
    $form['project_deliverables']['translated_file'][$file_id]['file_size'] = array('#value' => $translated_file->getSizeInKilobytes());
    $form['project_deliverables']['translated_file'][$file_id]['file_contents'] = array('#value' => $translated_file->getFileContents());
    $form['project_deliverables']['translated_file'][$file_id]['language_display'] = array('#value' => $translated_file->getLang()->getDisplay());
    $form['project_deliverables']['translated_file'][$file_id]['language_code'] = array('#value' => $translated_file->getLang()->getLanguageCode());
    $form['project_deliverables']['translated_file'][$file_id]['created_date'] = array('#value' => _format_display_date($translated_file->getCreatedDate()));
    $form['project_deliverables']['translated_file'][$file_id]['metadata_path'] = array('#value' => $translated_file->getPath());
    $form['project_deliverables']['translated_file'][$file_id]['content_path'] = array('#value' => $translated_file->getContentPath());
  }
  $form['project_deliverables']['project_id'] = array('#value' => $project->getId());
  $form['project_deliverables']['exists_master_translated_file'] = array('#value' => !empty($master_translated_file) ? true : false);
  $form['project_deliverables']['translated_files'] = $translated_file_list;
  $form['project_deliverables']['#theme'] = 'project_deliverables_theme';

  $form['submit'] = array('#type' => 'submit', '#value' => t('Back to Project List'));
  return $form;
}

function upload_materials_form($form_state, $project_id) {
  show_custom_messages($form);
  _setup_upload_source_materials_form($form, $project_id);
  _setup_upload_reference_materials_form($form);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Upload All Materials'));
  return $form;
}

function upload_source_materials_form($form_state, $project_id) {
  show_custom_messages($form);
  _setup_upload_source_materials_form($form, $project_id);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Upload Source Materials'));
  return $form;
}

function upload_reference_materials_form() {
  show_custom_messages($form);
  _setup_upload_reference_materials_form($form);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Upload Reference Materials'));
  return $form;
}

function configure_bid_options_form($form_state, $project_id) {
  show_custom_messages($form);
  try {
    $client = get_api_client();
    $form['bid_options'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fieldset-title">' . t('Who do you want to request quotes from for this project?') . '</span>',
      '#tree' => TRUE,
    );
    $preferred_vendors = $client->get_preferred_vendors();
    if( !empty($preferred_vendors) ) {
      $options = _get_preferred_vendor_options($preferred_vendors);
      $form['bid_options']['preferred_vendors'] = array(
        '#type' => 'markup',
        '#value' => '<div class="section-header">Select up to 5 of your preferred vendors (click on a vendor to select or deselect)</div>',
      );
      $form['bid_options']['preferred_vendors']['vendors'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#multiple' => true,
        '#size' => min(12, count($options)),
      );
    }
    $form['bid_options']['cloudwords_vendors'] = array(
      '#type' => 'markup',
      '#value' => '<div class="section-header">Include vendors chosen by Cloudwords</div>',
    );
    $form['bid_options']['cloudwords_vendors']['do_let_cloudwords_choose'] = array(
      '#type' => 'checkbox',
      '#title' => t('Let Cloudwords choose vendors for me'),
    );
    $form['bid_options']['#theme'] = 'configure_bid_options_theme';
    $form['submit'] = array('#type' => 'submit', '#value' => t('Finish and Request Bids'));
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
  }  
  return $form;
}

function import_master_file_form($form_state, $project_id) {
  show_custom_messages($form);
  try {
    $client = get_api_client();
    $dao = new CloudwordsDao();
    $project = $client->get_project($project_id);
    $form['import_file'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fieldset-title">' . t('Import Translated Materials') . '</span>',
      '#tree' => TRUE,
    );
    $form['import_file']['import_type'] = array('#type' => 'hidden', '#value' => 'master');
    // display a list of translated nodes that will be affected by the import
    $translated_node_ids = $dao->get_project_translated_nodes($project_id);
    if( !empty($translated_node_ids) ) {
      foreach( $translated_node_ids as &$node_id ) {
        $node = $dao->get_node_by_id($node_id);
        $translated_nodes[$node_id] = '';
        $form['import_file']['translated_node'][$node_id]['title'] = array('#value' => $node['title']);
        $form['import_file']['translated_node'][$node_id]['language'] = array('#value' => $client->lookup_language($node['language']));
        $form['import_file']['translated_node'][$node_id]['type'] = array('#value' => $node['type']);
        $form['import_file']['translated_node'][$node_id]['author'] = array('#value' => $node['name']);
      }
      $form['import_file']['import_type'] = array('#type' => 'hidden', '#value' => 'target_language');
      $form['import_file']['translated_nodes'] = $translated_nodes;
    }
    $form['import_file']['project_id'] = array('#type' => 'hidden', '#value' => $project_id);
    $form['import_file']['project_name'] = array('#type' => 'hidden', '#value' => $project->getName());
    $form['import_file']['#theme'] = 'project_translated_materials_theme';
    $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
  }
  return $form;
}

function import_language_file_form($form_state, $project_id, $language_code) {
  show_custom_messages($form);
  try {
    $client = get_api_client();
    $dao = new CloudwordsDao();
    $project = $client->get_project($project_id);
    $language_display = $client->lookup_language($language_code);
    $form['import_file'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fieldset-title">' . t('Import Translated Materials in ' . $language_display) . '</span>',
      '#tree' => TRUE,
    );
    $form['import_file']['import_type'] = array('#type' => 'hidden', '#value' => 'all_languages');
    // display a list of translated nodes that will be affected by the import
    $translated_node_ids = $dao->get_project_translated_nodes($project_id, $language_code);
    if( !empty($translated_node_ids) ) {
      foreach( $translated_node_ids as &$node_id ) {
        $node = $dao->get_node_by_id($node_id);
        $translated_nodes[$node_id] = '';
        $form['import_file']['translated_node'][$node_id]['title'] = array('#value' => $node['title']);
        $form['import_file']['translated_node'][$node_id]['language'] = array('#value' => $client->lookup_language($node['language']));
        $form['import_file']['translated_node'][$node_id]['type'] = array('#value' => $node['type']);
        $form['import_file']['translated_node'][$node_id]['author'] = array('#value' => $node['name']);
      }
      $form['import_file']['import_type'] = array('#type' => 'hidden', '#value' => 'target_language');
      $form['import_file']['translated_nodes'] = $translated_nodes;
    }
    $form['import_file']['project_id'] = array('#type' => 'hidden', '#value' => $project_id);
    $form['import_file']['project_name'] = array('#type' => 'hidden', '#value' => $project->getName());
    $form['import_file']['language_code'] = array('#type' => 'hidden', '#value' => $language_code);
    $form['import_file']['language_display'] = array('#type' => 'hidden', '#value' => $language_display);
    $form['import_file']['#theme'] = 'project_translated_materials_theme';
    $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
  }
  return $form;
}

/**
 * Private utility methods
 */

function _get_language_options($langs) {
  $options = array();
  foreach ($langs as &$lang) {
    $options[$lang->getLanguageCode()] = $lang->getDisplay();
  }
  return $options;
}

function _get_intended_use_options($intended_uses) {
  $options = array();
  foreach ($intended_uses as &$use) {
    $options[$use->getId()] = $use->getName();
  }
  return $options;
}

function _get_preferred_vendor_options($preferred_vendors) {
  $options = array();
  foreach ($preferred_vendors as &$vendor) {
    $options[$vendor->getId()] = $vendor->getName();
  }
  return $options;
}

function _format_display_date($date) {
  try {
    date_default_timezone_set('UTC');
    $datetime = new DateTime($date);
    return date('n/j/Y', $datetime->getTimestamp());  // returns mm/dd/yyyy format without leading zeros
  } catch( Exception $exc ) {
    return '';
  }
}

function _setup_upload_source_materials_form(&$form, $project_id) {
  $client = get_api_client();
  $project = $client->get_project($project_id);
  $form['source_materials'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fieldset-title">' . t('Upload Source Materials') . '</span>',
    '#tree' => TRUE,
  );
  $dao = new CloudwordsDao();
  $nodes = $dao->get_translatable_nodes($project->getSourceLanguage()->getLanguageCode());
  foreach( $nodes as $node ) {
    $node_id = $node['nid'];
    $checkboxes[$node_id] = '';
    $form['source_materials'][$node_id]['title'] = array('#value' => $node['title']);
    $form['source_materials'][$node_id]['language'] = array('#value' => $client->lookup_language($node['language']));
    $form['source_materials'][$node_id]['type'] = array('#value' => $node['type']);
    $form['source_materials'][$node_id]['author'] = array('#value' => $node['name']);
  }
  $form['source_materials']['source_language'] = array('#type' => 'hidden', '#value' => $project->getSourceLanguage()->getDisplay());
  $form['source_materials']['checkboxes'] = array('#type' => 'checkboxes', '#options' => $checkboxes);
  $form['source_materials']['#theme'] = 'upload_source_materials_theme';
}

function _setup_upload_reference_materials_form(&$form) {
  $form['reference_materials'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fieldset-title">' . t('Upload Reference Materials') . '</span>',
    '#tree' => TRUE,
  );
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['reference_materials']['reference_file'] = array(
    '#type' => 'file',
    '#title' => t('Select a reference file (Note: file must be a .zip file)'),
    '#size' => 30,
    '#maxlength' => 64,
  );
}

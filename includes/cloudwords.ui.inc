<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Utility functions used to import stylesheets and javascript libraries.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

function import_jquery_libraries() {
  drupal_add_js(drupal_get_path('module','cloudwords') . '/js/jquery-1.5.1.min.js');
  drupal_add_js(drupal_get_path('module','cloudwords') . '/js/jquery-ui-1.8.14.custom.min.js');
  drupal_add_css(drupal_get_path('module','cloudwords') . '/css/ui-lightness/jquery-ui-1.8.14.custom.css');
}

function import_custom_stylesheets() {
  drupal_add_css(drupal_get_path('module','cloudwords') . '/css/cloudwords.css');
}

function import_date_picker_widget() {
  import_jquery_libraries();
  drupal_add_js('jQuery(document).ready(function() { jQuery("#datepicker-delivery-due-date").datepicker(); jQuery("#datepicker-bid-due-date").datepicker(); });', 'inline');
}

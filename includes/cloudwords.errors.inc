<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Custom error message functions to report errors on forms.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

const MESSAGE_TYPE_CUSTOM = 'custom';

function set_custom_message($message) {
  drupal_get_messages();  // flush out all error messages so custom ones can be added
  drupal_set_message("<div class='message-custom'>" . $message . "</div>", MESSAGE_TYPE_CUSTOM);
}

function show_custom_messages(&$form) {
  $messages = drupal_get_messages(MESSAGE_TYPE_CUSTOM);
  if( !empty($messages) ) {
    $error_message = '';
    foreach( $messages as $message ) {
      $error_message .= '<br/>' . $message[0] . '<br/>'; 
    }
    $form['error_messages'] = array(
      '#type' => 'markup',
      '#value' => $error_message,
    );
  }
}

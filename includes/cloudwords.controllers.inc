<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Controller and form submit handler functions.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

require_once 'cloudwords.file.inc';
require_once 'cloudwords.xml.inc';

function admin_settings_form_submit($form, &$form_state) {
  $dao = new CloudwordsDao();
  $dao->set_configuration_settings($form);
  $form_state['redirect'] = ADMIN_SETTINGS_PATH;
  set_custom_message('Your configuration settings have been successfully saved');
}

function create_project_form_submit($form, &$form_state) {
  $params = array('name' => $form['project_details']['project_name']['#value'],
                  'description' => $form['project_details']['description']['#value'],
                  'notes' => $form['project_details']['notes']['#value'],
                  'bidDueDate' => _normalize_date($form['project_details']['bid_due_date']['#value']),
                  'poNumber' => $form['project_details']['purchase_order_number']['#value'],
                  'deliveryDueDate' => _normalize_date($form['project_details']['delivery_due_date']['#value']),
                  'intendedUse' => $form['project_details']['intended_use']['#value'],
                  'sourceLanguage' => $form['project_details']['source_language']['#value'],
                  'targetLanguages' => array_keys($form['project_details']['target_languages']['#value']));
  try {
    $client = get_api_client();
    $dao = new CloudwordsDao();
    $project = $client->create_project($params);
    $dao->create_project($project->getId(), $project->getSourceLanguage()->getLanguageCode());
    $dao->save_project_target_languages($project->getId(), $project->getTargetLanguages());
    $form_state['redirect'] = 'cloudwords/project/' . $project->getId() . '/file/upload';
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($exc->getErrorMessage());
    $form_state['redirect'] = false;
  }
}

function project_details_form_submit($form, &$form_state) {
  $form_state['redirect'] = VIEW_PROJECTS_PATH;
}

function upload_materials_form_submit($form, &$form_state) {
  // extract the project id from the url parameters
  $project_id = $form['#parameters'][2];
  try {
    // transform the selected nodes from the source materials table into xml
    $checkboxes = $form_state['values']['source_materials']['checkboxes'];
    $selected_nodes = _get_selected_source_materials($checkboxes);
    _upload_project_source_materials($project_id, $selected_nodes);

    // upload the selected reference materials zip archive
    $reference_file = $_FILES['files']['name']['reference_materials'];
    $reference_file_contents = $_FILES['files']['tmp_name']['reference_materials'];
    _upload_project_reference_materials($project_id, $reference_file, $reference_file_contents);

    $form_state['redirect'] = 'cloudwords/project/' . $project_id . '/configure/bids';
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($stored_exc->getErrorMessage());
    $form_state['redirect'] = false;
  }
}

function upload_source_materials_form_submit($form, &$form_state) {
  // extract the project id from the url parameters
  $project_id = $form['#parameters'][2];
  try {
    // transform the selected nodes from the source materials table into xml
    $checkboxes = $form_state['values']['source_materials']['checkboxes'];
    $selected_nodes = _get_selected_source_materials($checkboxes);
    _upload_project_source_materials($project_id, $selected_nodes);

    $form_state['redirect'] = 'cloudwords/project/' . $project_id . '/details';
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($stored_exc->getErrorMessage());
    $form_state['redirect'] = false;
  }
}

function upload_reference_materials_form_submit($form, &$form_state) {
  // extract the project id from the url parameters
  $project_id = $form['#parameters'][2];
  try {
    // upload the selected reference materials zip archive
    $reference_file = $_FILES['files']['name']['reference_materials'];
    $reference_file_contents = $_FILES['files']['tmp_name']['reference_materials'];
    _upload_project_reference_materials($project_id, $reference_file, $reference_file_contents);

    $form_state['redirect'] = 'cloudwords/project/' . $project_id . '/details';
  } catch( CloudwordsApiException $exc ) {
    set_custom_message($stored_exc->getErrorMessage());
    $form_state['redirect'] = false;
  }
}

function configure_bid_options_form_submit($form, &$form_state) {
  // extract the project id from the url parameters
  $project_id = $form['#parameters'][2];
  $preferred_vendors = !empty($form['bid_options']['preferred_vendors']['vendors']['#value']) ? array_keys($form['bid_options']['preferred_vendors']['vendors']['#value']) : array();
  $do_let_cloudwords_choose = $form['bid_options']['cloudwords_vendors']['do_let_cloudwords_choose']['#value'] == 0 ? false : true;
  try {
    $client = get_api_client();
    $client->request_bids_for_project($project_id, $preferred_vendors, $do_let_cloudwords_choose);
    $form_state['redirect'] = 'cloudwords/project/' . $project_id . '/details';
  } catch( CloudwordsApiException $exc ) {
    $stored_exc = $exc;
  }
  if( $stored_exc ) {
    set_custom_message($stored_exc->getErrorMessage());
    $form_state['redirect'] = false;
  }
}

function import_master_file_form_submit($form, &$form_state) {
  try {
    $project_id = $form['import_file']['project_id']['#value'];
    $client = get_api_client();

    // download the translated materials
    $data = $client->download_master_translated_file($project_id);
    
    // create a zip archive using the downloaded data
    $file_util = new FileUtility();
    $zip_file = $file_util->save_zip_archive($data);

    // unzip the translated materials
    $xml_files = $file_util->unzip_archive($zip_file);
    
    // ensure that each xml file conforms to the node schema
    $xml_util = new XMLUtility();
    $node_schema = drupal_get_path('module','cloudwords') . '/xml/node.xsd';
    $xml_util->validate_schema($xml_files, $node_schema);
    
    // import nodes into database
    $dao = new CloudwordsDao();
    foreach( $xml_files as $xml_file ) {
      // strip language code out of the filename
      $tokens = split('/', $xml_file);
      $target_language_code = $tokens[2];
      $imported_node = $xml_util->extract_xml_node_data($xml_file, $target_language_code);
      $dao->create_translated_node($imported_node, $target_language_code);
    }
    set_custom_message('All translated materials have been successfully imported');
    $form_state['redirect'] = 'cloudwords/project/' . $project_id . '/details';
  } catch( CloudwordsXmlException $exc ) {
    $stored_exc = $exc;
  } catch( CloudwordsApiException $exc ) {
    $stored_exc = $exc;
  }
  // clean up temporary files
  if( !empty($zip_file) ) {
    $file_util->delete_files($zip_file); 
  }
  if( !empty($xml_files) ) {
    $file_util->delete_files($xml_files);
  }
  if( $stored_exc ) {
    set_custom_message($stored_exc->getErrorMessage());
    $form_state['redirect'] = false;
  }
}

function import_language_file_form_submit($form, &$form_state) {
  try {
    $project_id = $form['import_file']['project_id']['#value'];
    $target_language_code = $form['import_file']['language_code']['#value'];
    $client = get_api_client();

    // download the translated materials
    $data = $client->download_translated_file($project_id, $target_language_code);
    
    // create a zip archive using the downloaded data
    $file_util = new FileUtility();
    $zip_file = $file_util->save_zip_archive($data);

    // unzip the translated materials
    $xml_files = $file_util->unzip_archive($zip_file);

    // ensure that each xml file conforms to the node schema
    $xml_util = new XMLUtility();
    $node_schema = drupal_get_path('module','cloudwords') . '/xml/node.xsd';
    $xml_util->validate_schema($xml_files, $node_schema);
    
    // import nodes into the database
    $dao = new CloudwordsDao();
    foreach( $xml_files as $xml_file ) {
      $imported_node = $xml_util->extract_xml_node_data($xml_file, $target_language_code);
      $dao->create_translated_node($imported_node, $target_language_code);
    }
    set_custom_message('Translated materials for ' . $client->lookup_language($target_language_code) . ' have been successfully imported');
    $form_state['redirect'] = 'cloudwords/project/' . $project_id . '/details';
  } catch( CloudwordsXmlException $exc ) {
    $stored_exc = $exc;
  } catch( CloudwordsApiException $exc ) {
    $stored_exc = $exc;
  }
  // clean up temporary files
  if( !empty($zip_file) ) {
    $file_util->delete_files($zip_file); 
  }
  if( !empty($xml_files) ) {
    $file_util->delete_files($xml_files);
  }
  if( $stored_exc ) {
    set_custom_message($stored_exc->getErrorMessage());
    $form_state['redirect'] = false;
  }  
}

/**
 * Private utility methods
 */

function _normalize_date($date) {
  try {
    date_default_timezone_set('UTC');
    $datetime = new DateTime($date);
    return date('c', $datetime->getTimestamp());  // returns ISO-8601 date format
  } catch( Exception $exc ) {
    return '';
  }
}

function _get_selected_source_materials($nodes) {
  $selected_nodes = array();
  foreach( $nodes as &$node_id ) {
    if( $node_id > 0 ) {
      $selected_nodes[] = $node_id;
    }
  }
  return $selected_nodes;
}

function _upload_project_source_materials($project_id, $nodes) {
  try {
    $client = get_api_client();
    $dao = new CloudwordsDao();
    $xml_util = new XMLUtility();
    $file_util = new FileUtility();
    $xml_files = array();
    // serialize each node into a temporary xml file
    foreach( $nodes as &$node_id ) {
      $db_node = $dao->get_node_by_id($node_id);
      $xml_files[] = $xml_util->serialize_node_contents($db_node);
    }
    if(!empty($xml_files)) {
      // create a zip archive containing the source materials
      $source_zip_file = $file_util->create_zip_archive($xml_files);
      // upload the source materials zip archive
      $client->upload_project_source($project_id, $source_zip_file);
      // link the source material nodes with the project
      $dao->insert_project_nodes($project_id, $nodes);
    }
  } catch( CloudwordsApiException $exc ) {
    $stored_exc = $exc;
  }
  if( !empty($xml_files) ) {
    $file_util->delete_files($xml_files);
  }
  if( !empty($source_zip_file) ) {
    $file_util->delete_files($source_zip_file); 
  }
  if( $stored_exc ) {
    throw $stored_exc;
  }  
}

function _upload_project_reference_materials($project_id, $reference_file, $reference_file_contents) {
  try {
    $client = get_api_client();
    $file_util = new FileUtility();
    if( !empty($reference_file) ) {
      $reference_zip_file = $file_util->save_uploaded_file($reference_file, $reference_file_contents);
      $client->upload_project_reference($project_id, $reference_zip_file);
    }
  } catch( CloudwordsApiException $exc ) {
    $stored_exc = $exc;
  }
  if( !empty($reference_zip_file) ) {
    $file_util->delete_files($reference_zip_file); 
  }
  if( $stored_exc ) {
    throw $stored_exc;
  }  
}

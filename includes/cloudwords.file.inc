<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Utility class containing methods to manage file resources.
 * 
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */
class FileUtility {

  const ZIP_FILE_SOURCE_PREFIX = 'src';
  const ZIP_FILE_EXTENSION = '.zip';

  /**
   * Creates a zip archive using the given list of files in the given destination directory. If no 
   * destination directory is specified, the zip archive is created in the temporary directory of 
   * the system.
   *
   * Notes: The created zip archive will overwrite any similarly named files that exist in the destination directory.
   *        The caller is responsible for removing the temporary zip archive from the system.
   *
   * @param array $files The list of files to place in the zip archive
   * @param string $destination optional The destination directory to create the zip archive
   *
   * @return mixed The path to the newly created zip archive or false if the zip archive could not be created
   */
  public function create_zip_archive($files, $destination = '') {
    // ensure a list of files has been provided and verify that all files exist
    $valid_files = array();
    if( is_array($files) ) {
      foreach( $files as $file ) {
        if( file_exists($file) ) {
          $valid_files[] = $file;
        } else {
          return false;
        }
      }
    }
    // create the zip archive
    if( count($valid_files) ) {
      $archiver = new ZipArchive();
      $zip_file = $this->generate_random_zip_archive_filename($destination);
      if( $archiver->open($zip_file, ZIPARCHIVE::OVERWRITE) !== true ) {
        return false;
      }
      foreach( $valid_files as $file ) {
        $archiver->addFile($file, $file);
      }
      $archiver->close();
      return $zip_file;
    }
    return false;
  }

  /**
   * Saves the given data as a zip archive in the given destination directory. If no destination directory 
   * is specified, the zip archive is saved in the temporary directory of the system. This function assumes 
   * the provided data is properly formatted zip file data.
   *
   * Notes: The saved zip archive will overwrite any similarly named files that exist in the destination directory.
   *        The caller is responsible for removing the temporary zip archive from the system.
   *
   * @param resource $data The zip file data to save as a zip archive
   * @param string $destination optional The destination directory to save the zip archive
   *
   * @return mixed The path to the zip archive or false if the zip archive could not be saved
   */
  public function save_zip_archive($data, $destination = '') {
    $zip_file = $this->generate_random_zip_archive_filename($destination);
    $status = file_put_contents($zip_file, $data, LOCK_EX);
    if( $status === false ) {
      return false;
    }
    return $zip_file;
  }

  /**
   * Saves the given uploaded data as a zip archive in the given destination directory. If no destination 
   * directory is specified, the zip archive is saved to the temporary directory of the system.
   *
   * Notes: The saved zip archive will overwrite any similarly named files that exist in the destination directory.
   *        The caller is responsible for removing the temporary zip archive from the system.
   *
   * @param string $filename The filename of the uploaded data file
   * @param resource $data The zip file data to save as a zip archive
   * @param string $destination optional The destination directory to store the contents of the zip archive
   *
   * @return mixed The path to the zip archive or false if the zip archive could not be saved
   */
  public function save_uploaded_file($filename, $data, $destination = '') {
    // determine the output directory of the zip archive contents
    if( empty($destination) ) {
      $destination = sys_get_temp_dir();
    }
    $output_file_path = $destination . '/' . $filename;
    $status = move_uploaded_file($data, $output_file_path);
    if( $status === false ) {
      return false;
    }
    return $output_file_path;
  }

  /**
   * Unzips the given zip archive into the given destination directory. If no destination directory is supplied, 
   * the zip archive will be unzipped in the temporary directory of the system.
   *
   * Notes: The contents of the zip archive will overwrite existing files in the temporary directory.
   *        The caller is responsible for removing the contents of the zip archive from the system.
   *
   * @param string $archive The full path to the zip archive to unzip
   * @param string $destination optional The destination directory to store the contents of the zip archive
   *
   * @return array The list of the files contained in the zip archive
   */  
  public function unzip_archive($archive, $destination = '') {
    // determine the output directory of the zip archive contents
    if( empty($destination) ) {
      $destination = sys_get_temp_dir();
    }
    // unzip the archive into the destination directory
    if( file_exists($archive) ) {
      $archiver = new ZipArchive();
      $status = $archiver->open($archive);
      if( $status === true ) {
        $status = $archiver->extractTo($destination);
        if( $status === true ) {
          $files = array();
          for( $index = 0; $index < $archiver->numFiles; $index++ ) {
            $stat = $archiver->statIndex($index);
            $output_file = $destination . '/' . $stat['name'];
            if( !is_dir($output_file) ) {
              $files[] = $output_file;
            }
          }
          $archiver->close();
          return $files;
        } else {
          $archiver->close();
          return false;
        }
      } else {
        $archiver->close();
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * Deletes the given list files or individual file.
   *
   * @param array $files A list of files or an individual file to be deleted
   *
   * @return boolean True if the files were successfully deleted and false otherwise
   */
  public function delete_files($files) {
    if( is_array($files) ) {
      foreach( $files as $file ) {
        unlink($file);
      }
    } else {
      unlink($files);
    }
  }

  /**
   * Generates a random zip archive filename using the given file prefix and file suffix in the 
   * temporary directory of the system.
   *
   * @param string $destination optional The destination directory to store the random zip archive
   * @param string $file_prefix optional The prefix to prepend to the random zip archive filename
   * @param string $file_extension optional The file extension to append to the random zip archive filename
   *
   * @return mixed The full path to the random zip archive file
   */
  public function generate_random_zip_archive_filename($destination = '', $file_prefix = 'src', $file_extension = '.zip') {
    // determine the output directory of the zip archive contents
    if( empty($destination) ) {
      $destination = sys_get_temp_dir();
    }
    return tempnam($destination, $file_prefix) . $file_extension;
  }

  /**
   * Private helper methods
   */

  private function startsWith($haystack, $needle) {
    return strncmp($haystack, $needle, strlen($needle)) == 0;
  }

  private function endsWith($haystack, $needle) {
    return $this->startsWith(strrev($haystack), strrev($needle));
  }
  
}

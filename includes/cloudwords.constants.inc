<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Constants defining all available paths in the module
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

const ADMIN_SETTINGS_PATH = 'admin/settings/cloudwords';
const CREATE_PROJECT_PATH = 'cloudwords/project/create';
const VIEW_PROJECTS_PATH = 'cloudwords/projects/view';
const PROJECT_DETAILS_PATH = 'cloudwords/project/%/details';
const UPLOAD_FILE_PATH = 'cloudwords/project/%/file/upload';
const UPLOAD_SOURCE_FILE_PATH = 'cloudwords/project/%/file/upload/source';
const UPLOAD_REFERENCE_FILE_PATH = 'cloudwords/project/%/file/upload/reference';
const CONFIGURE_BIDS_PATH = 'cloudwords/project/%/configure/bids';
const IMPORT_MASTER_FILE_PATH = 'cloudwords/project/%/file/import/master';
const IMPORT_LANGUAGE_FILE_PATH = 'cloudwords/project/%/file/import/language/%';

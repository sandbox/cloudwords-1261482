<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Theming functions to style forms and response pages.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

require_once 'cloudwords.ui.inc';

import_custom_stylesheets();

function cloudwords_theme() {
  return array(
    'project_list_theme' => array(
      'arguments' => array('form' => NULL),
    ),
    'project_details_theme' => array(
      'arguments' => array('form' => NULL),
    ),
    'project_source_materials_theme' => array(
      'arguments' => array('form' => NULL),
    ),
    'project_translated_materials_theme' => array(
      'arguments' => array('form' => NULL),
    ),
    'project_reference_materials_theme' => array(
      'arguments' => array('form' => NULL),
    ),
    'project_deliverables_theme' => array(
      'arguments' => array('form' => NULL),
    ),
    'upload_source_materials_theme' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

function theme_project_list_theme($form) {
  $rows = array();
  foreach(element_children($form['project_list']) as $project_id) {
    $row = array();
    $row[] = l($form[$project_id]['project_name']['#value'], 'cloudwords/project/'. $project_id . '/details');
    $row[] = drupal_render($form[$project_id]['project_status']);
    $rows[] = $row;
  }
  $header = array(t('Project Name'), t('Status'));
  if(count($rows) <= 0) {
    $row = array();
    $row[] = array(
      'data' => t('No projects to be viewed in this category'),
      'colspan' => count($header),
      'style' => 'text-align:center',
    );
    $rows[] = $row;
  }
  $table = theme('table', $header, $rows);
  $table = str_replace('<thead>', '<thead class="tableHeader-processed">', $table);  // disable sticky table header
  return $table;
}

function theme_project_details_theme($form) {
  $pending_actions = _get_pending_actions($form['metadata']['project_id']['#value'], $form['metadata']['status_code']['#value']);
  $project_details = $pending_actions . 
              '<div class="column">' . 
              '  <div class="row"><span class="row-label">Status:</span><span class="row-value">' . $form['metadata']['status']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Source Language:</span><span class="row-value">' . $form['metadata']['source_language']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Target Languages:</span><span class="row-value">' . $form['metadata']['target_languages']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Intended Use:</span><span class="row-value">' . $form['metadata']['intended_use']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Purchase Order #:</span><span class="row-value">' . $form['metadata']['purchase_order_number']['#value'] . '</span></div>' .
              '</div>' . 
              '<div class="column">' .
              '  <div class="row"><span class="row-label">Created On:</span><span class="row-value">' . $form['metadata']['created_date']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Bid Due Date:</span><span class="row-value">' . $form['metadata']['bid_due_date']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Bid Selection Deadline:</span><span class="row-value">' . $form['metadata']['bid_selection_deadline']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Delivery Due Date:</span><span class="row-value">' . $form['metadata']['delivery_due_date']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Amount:</span><span class="row-value">' . $form['metadata']['amount']['#value'] . '</span></div>' . 
              '</div>' .
              '<div class="container">' .
              '  <div class="row"><span class="row-label">Description:</span><span class="row-value">' . $form['metadata']['description']['#value'] . '</span></div>' . 
              '  <div class="row"><span class="row-label">Notes and Instructions:</span><span class="row-value">' . $form['metadata']['notes']['#value'] . '</span></div>';
              '</div>';
  return $project_details;
}

function theme_project_source_materials_theme($form) {
  $header = array(t('Title'), t('Language'), t('Type'), t('Author'));
  $source_nodes = element_children($form['source_nodes']);
  if( !empty($source_nodes) ) {
    $rows = array();
    foreach( $source_nodes as $node_id ) {
      $row = array();
      $row[] = l($form['source_node'][$node_id]['title']['#value'], 'node/'. $node_id);
      $row[] = drupal_render($form['source_node'][$node_id]['language']);
      $row[] = drupal_render($form['source_node'][$node_id]['type']);
      $row[] = drupal_render($form['source_node'][$node_id]['author']);
      $rows[] = $row;
    }
  } else {
    $row = array();
    $row[] = array(
      'data' => t('No source materials exist for this project' . '<br/>' . l('Upload Source Materials', 'cloudwords/project/' . $form['project_id']['#value'] . '/file/upload/source')),
      'colspan' => count($header),
      'style' => 'text-align:center',
    );
    $rows[] = $row;
  }
  $table = theme('table', $header, $rows);
  $table = str_replace('<thead>', '<thead class="tableHeader-processed">', $table);  // disable sticky table header
  return $table;
}

function theme_project_reference_materials_theme($form) {
  $header = array(t('Filename'), t('File Size (KB)'), t('Created On'));
  $reference_files = element_children($form['reference_files']);
  if( !empty($reference_files) ) {
    $rows = array();
    foreach( $reference_files as $file_id ) {
      $row = array();
      $row[] = drupal_render($form['reference_file'][$file_id]['filename']);
      $row[] = drupal_render($form['reference_file'][$file_id]['file_size']);
      $row[] = drupal_render($form['reference_file'][$file_id]['created_date']);
      $rows[] = $row;
    }
  } else {
    if(count($rows) <= 0) {
      $row = array();
      $row[] = array(
        'data' => t('No reference materials exist for this project'),
        'colspan' => count($header),
        'style' => 'text-align:center',
      );
      $rows[] = $row;
    }
  }
  $table = theme('table', $header, $rows);
  $table = str_replace('<thead>', '<thead class="tableHeader-processed">', $table);  // disable sticky table header
  $upload_link = '<div class="center">' . l('Upload Reference Materials', 'cloudwords/project/' . $form['project_id']['#value'] . '/file/upload/reference') . '</div>';
  return $table . $upload_link;
}

function theme_project_translated_materials_theme($form) {
  $header = array(t('Title'), t('Language'), t('Type'), t('Author'));
  $project_name = $form['project_name']['#value'];
  $language_display = $form['language_display']['#value'];
  $import_type = $form['import_type']['#value'];
  switch( $import_type ) {
    case "master":
      $confirmation_message = "<div>Are you sure you want to import all translated materials for Project '" . $project_name . "'?<br/>" . 
                              "All translated materials will be automatically imported and viewable in the content management area.</div><br/>";
      break;
    case "all_languages":
      $confirmation_message = "<div>Are you sure you want to import the translated materials in <b>" . $language_display . "</b> for Project '" . $project_name . "'?<br/>" .
                              "Translated materials for <b>" . $language_display . "</b> will be automatically imported and viewable in the content management area.</div><br/>"; 
      break;
    case "target_language":
      $confirmation_message = "<div>Are you sure you want to import the translated materials in <b>" . $language_display . "</b> for Project '" . $project_name . "'?<br/>" . 
                              "<b>WARNING:</b> Importing the translated materials will overwrite all of the following content listed below:</div><br/>";
      break;
  };
  $output = $confirmation_message;
  $translated_nodes = element_children($form['translated_nodes']);
  if( !empty($translated_nodes) ) {
    $rows = array();
    foreach( $translated_nodes as $node_id ) {
      $row = array();
      $row[] = l($form['translated_node'][$node_id]['title']['#value'], 'node/'. $node_id);
      $row[] = drupal_render($form['translated_node'][$node_id]['language']);
      $row[] = drupal_render($form['translated_node'][$node_id]['type']);
      $row[] = drupal_render($form['translated_node'][$node_id]['author']);
      $rows[] = $row;
    }
    $table = theme('table', $header, $rows);
    $table = str_replace('<thead>', '<thead class="tableHeader-processed">', $table);  // disable sticky table header
    $output .= $table;
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  return $output;
}

function theme_project_deliverables_theme($form) {
  $header = array(t('Language'), t('File Size (KB)'), t('Created On'), t('Action'));
  $exists_master_translated_file = $form['exists_master_translated_file']['#value'];
  if( $exists_master_translated_file ) {
    $rows = array();
    $translated_files = element_children($form['translated_files']);
    foreach( $translated_files as $file_id) {
      $row = array();
      $row[] = drupal_render($form['translated_file'][$file_id]['language_display']);
      $row[] = drupal_render($form['translated_file'][$file_id]['file_size']);
      $row[] = drupal_render($form['translated_file'][$file_id]['created_date']);
      $row[] = l('Import', 'cloudwords/project/' . $form['project_id']['#value'] . '/file/import/language/' . $form['translated_file'][$file_id]['language_code']['#value']);
      $rows[] = $row;
    }
    $import_all_link = '<div class="center">' . l('Import All Deliverables', 'cloudwords/project/' . $form['project_id']['#value'] . '/file/import/master') . '</div>';
  } else {
    if(count($rows) <= 0) {
      $row = array();
      $row[] = array(
        'data' => t('No translated materials are ready to be imported for this project'),
        'colspan' => count($header),
        'style' => 'text-align:center',
      );
      $rows[] = $row;
    }
  }
  $table = theme('table', $header, $rows);
  $table = str_replace('<thead>', '<thead class="tableHeader-processed">', $table);  // disable sticky table header
  return $table . $import_all_link;
}

function theme_upload_source_materials_theme($form) {
  $rows = array();
  foreach(element_children($form['checkboxes']) as $node_id) {
    $row = array();
    $row[] = drupal_render($form['checkboxes'][$node_id]);
    $row[] = drupal_render($form[$node_id]['title']);
    $row[] = drupal_render($form[$node_id]['language']);
    $row[] = drupal_render($form[$node_id]['type']);
    $row[] = drupal_render($form[$node_id]['author']);
    $rows[] = $row;
  }
  if(count($rows) > 0) {
    $header = array(theme('table_select_header_cell'), t('Title'), t('Language'), t('Type'), t('Author'));
  }
  else {
    $header = array(t('Title'), t('Language'), t('Type'), t('Author'));
    $row = array();
    $row[] = array(
      'data' => t('No ' . $form['source_language']['#value'] . ' content is available for upload'),
      'colspan' => count($header),
      'style' => 'text-align:center',
    );
    $rows[] = $row;
  }
  $table = theme('table', $header, $rows);
  $table = str_replace('<thead>', '<thead class="tableHeader-processed">', $table);  // disable sticky table header
  return drupal_render($form) . $table; 
}

function _get_pending_actions($project_id, $status_code) {
  switch($status_code) {
    case "configured_project_details":
      $pending_action_link = l('Upload Source Materials', 'cloudwords/project/'. $project_id . '/file/upload/source');
      break;
    case "uploaded_source_materials":
      $pending_action_link = l('Request Bids', 'cloudwords/project/'. $project_id . '/configure/bids');
      break;
    default:
      $pending_actions = '';
  }
  if( !empty($pending_action_link) ) {
    $pending_actions = '<div class="container">' .
                       '  <div class="row"><span class="row-label">Pending Action:</span><span class="row-value">' . $pending_action_link . '</span></div>' .
                       '</div>';
  }
  return $pending_actions;
}

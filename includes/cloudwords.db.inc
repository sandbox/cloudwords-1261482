<?php

/*
 * Copyright 2011, Cloudwords, Inc.
 *
 * Licensed under the API LICENSE AGREEMENT, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.cloudwords.com/developers/license-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Database hooks and data access object functions.
 *
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */

const DEFAULT_BASE_API_URL = 'https://api.cloudwords.com';
const DEFAULT_API_VERSION = 1;
const DRUPAL_NODE_TABLE = 'node';
const CW_AUTH_TABLE = 'cw_auth';
const CW_PROJECT_TABLE = 'cw_project';
const CW_PROJECT_LANGUAGE_TABLE = 'cw_project_language';
const CW_PROJECT_NODE_TABLE = 'cw_project_node';

/**
 * Implements hook_enable().
 * 
 * Creates all tables defined in hook_schema().
 *
 */
function cloudwords_enable() {
  drupal_install_schema('cloudwords');
}

/**
 * Implements hook_disable().
 *
 * Removes all tables defined in hook_schema().
 *
 */
function cloudwords_disable() {
  drupal_uninstall_schema('cloudwords');
}

/**
 * Implements hook_schema().
 */
function cloudwords_schema() {
  $schema = array();
  $schema[CW_AUTH_TABLE] = array(
      'description' => 'Cloudwords user authorization database schema',
      'fields' => array(
          'id' => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
          'uid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE),
          'base_api_url' => array('type' => 'char', 'length' => '64', 'not null' => TRUE, 'default' => DEFAULT_BASE_API_URL),
          'api_version' => array('type' => 'int', 'length' => '64', 'not null' => TRUE, 'default' => DEFAULT_API_VERSION),
          'auth_token' => array('type' => 'char', 'length' => '64', 'not null' => TRUE, 'default' => ''),
      ),
      'primary key' => array('id'),
      'unique keys' => array(
        'uid' => array('uid')
      ),
  );
  $schema[CW_PROJECT_TABLE] = array(
      'description' => 'Cloudwords project database schema', 
      'fields' => array( 
          'id' => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE ), 
          'pid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE ), 
          'uid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE ),
          'language' => array('type' => 'char', 'length' => '7', 'not null' => TRUE ),
      ),
      'primary key' => array('id'),
      'unique keys' => array(
        'id' => array('id')
      ),
  );
  $schema[CW_PROJECT_LANGUAGE_TABLE] = array(
      'description' => 'Cloudwords project target languages database schema', 
      'fields' => array( 
          'id' => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE ), 
          'pid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE ),
          'language' => array('type' => 'char', 'length' => '7', 'not null' => TRUE ),
      ),
      'primary key' => array('id'),
      'unique keys' => array(
        'id' => array('id')
      ),
  );
  $schema[CW_PROJECT_NODE_TABLE] = array(
      'description' => 'Cloudwords project node database schema', 
      'fields' => array( 
          'id' => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE ), 
          'pid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE ),
          'nid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE ),
      ),
      'primary key' => array('id'),
  );
  return $schema;
}

/**
 * Data access object class containing methods to query and update the cloudwords database.
 * 
 * @author Douglas Kim <doug@cloudwords.com>
 * @since 1.0
 */
class CloudwordsDao {

  /**
   * Retrieves configuration settings from the database.
   * 
   * @return array A list containing the configuration settings
   */
  public function get_configuration_settings() {
    return db_fetch_array(db_query('SELECT base_api_url, api_version, auth_token FROM {' . CW_AUTH_TABLE . '}'));
  }

  /**
   * Inserts or updates configuration settings in the database.
   * 
   * @return boolean Success or failure if the configuration settings have been saved or updated
   */
  public function set_configuration_settings($form) {
    $status = false;
    global $user;
    $data = array('uid' => $user->uid,
                  'base_api_url' => $form['admin_settings']['base_api_url']['#value'],
                  'api_version' => $form['admin_settings']['api_version']['#value'],
                  'auth_token' => $form['admin_settings']['auth_token']['#value']);
    if( db_result(db_query('SELECT id FROM {' . CW_AUTH_TABLE . '} WHERE uid = %d', $user->uid)) ) {
      // update authorization token
      $status = drupal_write_record(CW_AUTH_TABLE, $data, array('uid'));
    } else {
      // insert authorization token
      $status = drupal_write_record(CW_AUTH_TABLE, $data);
    }
    return $status === false ? false : true;
  }

  /**
   * Inserts rows into the project table to track all newly created projects.
   * 
   * @param int $project_id The project id
   * @param string $source_language The source language code
   *
   * @return boolean Success or failure if the project has been created
   */
  public function create_project($project_id, $source_language) {
    global $user;
    $status = false;
    $data = array('pid' => $project_id, 'uid' => $user->uid, 'language' => $source_language);
    if( !db_result(db_query('SELECT id FROM {' . CW_PROJECT_TABLE . '} WHERE pid = %d', $project_id)) ) {
      $status = drupal_write_record(CW_PROJECT_TABLE, $data);
    }
    return $status === false ? false : true;
  }

  /**
   * Inserts rows into the project language table to track target languages for all projects.  
   *
   * @param int $project_id The project id
   * @param array $languages The list of target languages associated with the given project
   * 
   * @return boolean Success if all the target langauges have been saved or failure if any of the target languages could not be saved
   */
  public function save_project_target_languages($project_id, $languages) {
    $status = false;
    foreach( $languages as $language ) {
      $language_code = $language->getLanguageCode();
      if( !db_result(db_query("SELECT id FROM {" . CW_PROJECT_LANGUAGE_TABLE . "} WHERE pid = %d AND language = '%s'", $project_id, $language_code)) ) {
        $data = array('pid' => $project_id, 'language' => $language_code);
        $status = drupal_write_record(CW_PROJECT_LANGUAGE_TABLE, $data);
      }
    }
    return $status === false ? false : true;
  }

  /**
   * Retrieves a list of target language codes associated with the given project.
   *
   * @param int $project_id The project id
   */
  public function get_project_target_languages($project_id) {
    $target_languages = array();
    $results = db_query('SELECT language FROM {' . CW_PROJECT_LANGUAGE_TABLE . '} WHERE pid = %d', $project_id);
    while( $lang = db_fetch_object($results) ) {
      $target_languages[] = $lang->{'language'};
    }
    return $target_languages;
  }

  /**
   * Inserts rows into the project node table to link uploaded source material nodes to the given project.
   * 
   * Notes: Source material nodes that are already tied to the given project are not overwritten.
   *
   * @param int $project_id The project id
   * @param array $nodes A list of node ids
   *
   * @return boolean Success or failure if the source material nodes have been inserted
   */
  public function insert_project_nodes($project_id, $nodes) {
    $status = false;
    foreach( $nodes as $node_id ) {
      if( !db_result(db_query('SELECT id FROM {' . CW_PROJECT_NODE_TABLE . '} WHERE pid = %d AND nid = %d', $project_id, $node_id)) ) {
        $data = array('pid' => $project_id, 'nid' => $node_id);
        $status = drupal_write_record(CW_PROJECT_NODE_TABLE, $data);
      }
    }
    return $status === false ? false : true;
  }

  /**
   * Retrieves a list of source material nodes tied to the given project.
   * 
   * @param int $project_id The project id
   *
   * @return array A list containing a list of source material node ids tied to the given project
   */
  public function get_project_nodes($project_id) {
    $project_nodes = array();
    $results = db_query("SELECT nid FROM {" . CW_PROJECT_NODE_TABLE . "} WHERE pid = %d", $project_id);
    while( $node = db_fetch_object($results) ) {
      $project_nodes[] = $node->{'nid'};
    }
    return $project_nodes;
  }

  /**
   * Filters the given list of projects to projects that have only been created using this module.
   * 
   * @param array $projects The list of projects to filter
   *
   * @return array A filtered list of projects that have been created using this module
   */
  public function filter_projects($projects) {
    global $user;
    $filtered_projects = array();
    foreach( $projects as $project ) {
      if( db_result(db_query('SELECT id FROM {' . CW_PROJECT_TABLE . '} WHERE pid = %d AND uid = %d', $project->getId(), $user->uid)) ) {
        $filtered_projects[] = $project;
      }
    }
    return $filtered_projects;
  }

  /**
   * Creates a new node in the drupal node table using the given imported node and target language code.
   * If a translated node already exists that matches the given imported node, the translated node will 
   * be updated.
   * 
   * @param array $imported_node The imported node to be inserted or updated
   * @param string $language_code The language code that the given node has been translated
   *
   * @return mixed The newly created translated node; false otherwise
   */
  public function create_translated_node($imported_node, $language_code) {
    $translated_node = db_fetch_array(db_query("SELECT nid FROM {" . DRUPAL_NODE_TABLE . "} WHERE language = '%s' AND tnid = %d", $language_code, $imported_node['nid']));
    if( empty($translated_node) ) {
      $this->add_translated_node($imported_node);
    } else {
      $this->update_translated_node($translated_node);
    }
    /* Link imported translated node to its source node by setting a non-zero translated node id for the source node.
     * Make sure not to overwrite the translated node id for the source node if this field is non-zero. A non-zero 
     * translated node id field means the source node is itself a translated node of a different source node.
     */
    return db_query("UPDATE {" . DRUPAL_NODE_TABLE . "} SET tnid = %d WHERE nid = %d AND tnid = 0", $imported_node['nid'], $imported_node['nid']);
  }

  /**
   * Retrieves a list of translated nodes for the given project. The list contain translated nodes in the specified
   * target_language if one is provided. Otherwise the list will contain all translated nodes associated with the 
   * entire project.
   * 
   * @param int $project_id The project id
   * @param string $target_language optional The target language to retrieve nodes for
   * 
   * @return array A list of node ids that have been translated in the given target language for the given project
   */
  public function get_project_translated_nodes($project_id, $target_langauge = '') {
    $translated_node_ids = array();
    $sql = "SELECT n.nid FROM {" . DRUPAL_NODE_TABLE . "} n, {" . CW_PROJECT_NODE_TABLE . "} pn WHERE pn.pid = %d AND pn.nid = n.tnid AND language = '%s'"; 
    if( !empty($language_code) ) {
      $results = db_query($sql, $project_id, $language_code);
      while( $node = db_fetch_object($results) ) {
        $translated_node_ids[] = $node->{'nid'};
      }
    } else {
      $target_languages_codes = $this->get_project_target_languages($project_id);
      foreach( $target_languages_codes as $target_language_code ) {
        $results = db_query($sql, $project_id, $target_language_code);
        while( $node = db_fetch_object($results) ) {
          $translated_node_ids[] = $node->{'nid'};
        }
      }
    }
    return $translated_node_ids;
  }

  /**
   * Retrieves a list of nodes that are translatable based on the given source language
   * 
   * @param string $source_langauge The source language code to retrieve nodes for
   *  
   * @return array A list of translatable node ids
   */
  public function get_translatable_nodes($source_language) {
    $translatable_nodes = array();
    $results = db_query("SELECT nid FROM {" . DRUPAL_NODE_TABLE . "} WHERE language = '%s'", $source_language);
    while( $node = db_fetch_object($results) ) {
      $node = $this->get_node_by_id($node->{'nid'});
      $translatable_nodes[] = $node;
    }
    return $translatable_nodes;
  }

  /**
   * Retrieves a specific node from the drupal node table for the given node id.
   * 
   * @param int $node_id The node id
   *    
   * @return array The metadata for the node
   */
  public function get_node_by_id($node_id) {
    $node = node_load($node_id);
    return $this->convert_object_to_array($node);
  }

  /**
   * Retrieves a list of all node ids from the drupal node table.
   * 
   * @return array A list of all nodes ids 
   */
  private function get_all_node_ids() {
    $node_ids = array();
    $results = db_query('SELECT nid FROM {' . DRUPAL_NODE_TABLE . '}');
    while( $node = db_fetch_object($results) ) {
      $node_ids[] = $node->{'nid'};
    }
    return $node_ids;
  }

  /**
   * Adds a new translated node to the drupal node table. 
   * 
   * @param array $translated_node The translated node metadata
   *
   */
  private function add_translated_node($translated_node) {
    $new_node = $this->convert_array_to_object($translated_node);
    $new_node->tnid = $translated_node['nid'];  // set the translated node id column to the source node id
    $new_node->nid = '';                        // unsetting the node id creates a new node
    node_save($new_node);
  }

  /**
   * Updates an existing translated node to the drupal node table. 
   * 
   * @param array $translated_node The translated node metadata
   *
   */
  private function update_translated_node($translated_node) {
    $new_node = $this->convert_array_to_object($translated_node);
    $new_node->tnid = $translated_node['nid'];  // set the translated node id column to the source node id
    $new_node->nid = $translated_node['nid'];   // setting the node id updates an existing node
    node_save($new_node);
  }

  /*
   * Utility serialization methods 
   */

  private function convert_object_to_array($obj) {
    return get_object_vars($obj);
  }

  private function convert_array_to_object($array) {
    return (object)$array;
  }

}
